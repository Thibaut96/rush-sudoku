/*
** sudok.c for rush sudoku in /home/chauvo_a/rendu/sudoki-bi
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Sat Feb 27 00:53:51 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:51:13 2016 Thibaut Chauvot
*/

#include <stdlib.h>
#include <stdio.h>
#include "include/sudoku.h"

int		check_number(t_tab *tab, int pos)
{
  tab->nb = tab->tab[pos/9][pos%9] + 1;
  tab->c = pos % 9;
  tab->l = pos / 9;
  while (tab->nb < 10)
    {
      if (check_line(tab) == 0)
	if (check_col(tab) == 0)
	  if (check_square(tab) == 0)
	    return (0);
      tab->nb++;
    }
  return (1);
}

int		get_sudok(t_tab *tab)
{
  int		pos;

  pos = tab->pos;
  if (tab->base_tab[pos/9][pos%9] == 0)
    {
      if (check_number(tab, pos) == 0)
	{
	  tab->tab[pos/9][pos%9] = tab->nb;
	  tab->z = 0;
	  return (0);
	}
      else
	{
	  tab->tab[pos/9][pos%9] = 0;
	  tab->z = 1;
	  return (1);
	}
    }
  if (tab->z == 0)
    return (0);
  else
    return (1);
}
