/*
** main.c for Rush Sudoku in /home/chauvo_a/rendu/sudoki-bi
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Fri Feb 26 20:14:06 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:52:36 2016 Thibaut Chauvot
*/

#include <stdlib.h>
#include <stdio.h>
#include "include/sudoku.h"

int		print(t_tab *tab)
{
  int		i;
  int		y;

  y = 0;
  my_putstr("|------------------|\n");
  while (y < 9)
    {
      i = 0;
      my_putchar('|');
      while (i < 9)
	{
	  my_putchar(' ');
	  my_putchar(tab->tab[y][i] + 48);
	  i++;
	}
      my_putchar('|');
      my_putchar('\n');
      y++;
    }
  my_putstr("|------------------|\n");
}

int		create_tab(t_tab *tab)
{
  int		i;

  i = 0;
  if ((tab->tab = malloc(sizeof(char *) * 9)) == NULL)
    return (1);
  if ((tab->base_tab = malloc(sizeof(char *) * 9)) == NULL)
    return(1);
  while (i < 9)
    {
      if ((tab->tab[i] = malloc(10)) == NULL)
	return (1);
      if ((tab->base_tab[i] = malloc(10)) == NULL)
      	return (1);
      i++;
    }
  return (0);
}

int		make_tab(t_tab *tab, char **av)
{
  int		i;
  int		y;
  int		x;

  y = 0;
  get_line(0);
  while ((tab->str = get_line(0)) != NULL)
    {
      x = 2;
      i = 0;
      while (i < 9)
	{
	  in_tab(tab, y, i, x);
	  i++;
	  x = x + 2;
	}
      tab->tab[y][i] = 0;
      y++;
    }
  return (0);
}

int		in_tab(t_tab *tab, int y, int i, int x)
{
  if (tab->str[x] != ' ')
    {
      tab->base_tab[y][i] = tab->str[x] - 48;
      tab->tab[y][i] = tab->str[x] - 48;
    }
  else if (tab->str[x] == ' ')
    {
      tab->tab[y][i] = 0;
      tab->base_tab[y][i] = 0;
    }
  return (0);
}

int		get_map(char **av, t_tab *tab)
{
  if ((create_tab(tab)) == 1)
    return (1);
  tab->z = 0;
  tab->pos = 0;
  make_tab(tab, av);
  while (tab->pos < 81)
    {
      if (get_sudok(tab) == 0)
	tab->pos++;
      else
	tab->pos--;
    }
  print(tab);
  return (0);
}

int		main(int ac, char **av)
{
  t_tab		*tab;

  if ((tab = malloc(sizeof(t_tab))) == NULL)
    return (1);
  get_map(av, tab);
  return (0);
}
