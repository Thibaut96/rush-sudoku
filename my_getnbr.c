/*
** gnb.c for getnbr in /home/chauvo_a/rendu/CPE_2015_Allum1
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Wed Feb 17 22:53:28 2016 Thibaut Chauvot
** Last update Fri Feb 26 23:29:19 2016 Thibaut Chauvot
*/

#include <stdio.h>
#include <stdlib.h>
#include "include/sudoku.h"

int		my_getnbr(char *str)
{
  int		i;
  int		res;

  res = 0;
  i = 0;
  while (str[i] != '\n')
    {
      if (i == 10)
	return (15);
      if ((str[i] < '0' || str[i] > '9') && str[i] != '\n')
	return (-1);
      res = (res * 10) + (str[i] - 48);
      i++;
    }
  if (i == 0 && str[i] == '\n')
    return (-1);
  return (res);
}
