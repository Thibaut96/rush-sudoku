/*
** check_tab.c for rush sudoku in /home/chauvo_a/rendu/sudoki-bi
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Sat Feb 27 00:07:00 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:51:41 2016 Thibaut Chauvot
*/

#include <stdlib.h>
#include <stdio.h>
#include "include/sudoku.h"

int		check_line(t_tab *tab)
{
  int		i;

  i = 0;
  while (i < 9)
    {
      if ((tab->tab[tab->l][i]) == tab->nb)
	return (1);
      i++;
    }
  return (0);
}

int		check_col(t_tab *tab)
{
  int		i;

  i = 0;
  while (i < 9)
    {
      if ((tab->tab[i][tab->c]) == tab->nb)
	return (1);
      i++;
    }
  return (0);
}

int		check_square(t_tab *tab)
{
  int		c;
  int		l;
  int		y;
  int		i;

  y = 0;
  c = (tab->c - (tab->c % 3));
  while (y < 3)
    {
      i = 0;
      l = (tab->l - (tab->l % 3));
      while (i < 3)
	{
	  if ((tab->tab[l][c]) == tab->nb)
	    return (1);
	  l++;
	  i++;
	}
      c++;
      y++;
    }
  return (0);
}
