/*
** my_putnbr.c for putnbr in /home/chauvo_a/rendu/PSU_2015_minitalk
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Wed Jan 27 18:55:38 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:50:25 2016 Thibaut Chauvot
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "include/sudoku.h"

char		*my_strcpy(char *dest, char *src)
{
  int		i;

  i = 0;
  if (src == NULL)
    return (dest);
  while (src[i] != '\0' && (src + i) != NULL )
    {
      dest[i] = src[i];
      i = i + 1;
    }
  dest[i] = '\0';
  return (dest);
}

int		my_putchar(char c)
{
  if ((write(1, &c, 1)) == -1)
    return (1);
  return (0);
}

int		my_putstr(char *str)
{
  int		i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i++;
    }
  return (0);
}

int		my_putnbr(int nb)
{
  int		div;

  div = 1;
  if (nb < 0)
    {
      my_putchar('-');
      nb = nb * (- 1);
    }
  while ((nb / div) > 9)
    div = div * 10;
  while (div > 0)
    {
      my_putchar((nb / div) % 10 + 48);
      div = div / 10;
    }
  return (0);
}
