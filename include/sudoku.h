/*
** sudoku.h for rush sudoku in /home/chauvo_a/rendu/sudoki-bi/include
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Fri Feb 26 23:05:47 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:51:27 2016 Thibaut Chauvot
*/

#ifndef SUDOKU_H_
# define SUDOKU_H_

typedef struct	s_tab
{
  char		**tab;
  char		**base_tab;
  int		l;
  int		c;
  int		nb;
  int		z;
  int		pos;
  char		*str;
}		t_tab;

char		*get_line(const int fd);
char		*my_strcpy(char *dest, char *src);
int		my_putchar(char c);
int		my_putnbr(int nb);
char		*my_cat(char *str, char *buffer, int n);
int		my_getnbr(char *str);
int		print(t_tab *tab);
int		create_tab(t_tab *tab);
int		make_tab(t_tab *tab, char **av);
int		in_tab(t_tab *tab, int y, int i, int x);
int		get_map(char **av, t_tab *tab);
int		check_line(t_tab *tab);
int		check_col(t_tab *tab);
int		check_square(t_tab *tab);
int		check_number(t_tab *tab, int pos);
int		get_sudok(t_tab *tab);

#endif /* !SUDOKU_H_ */
