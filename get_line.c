/*
** get_line.c for minishell1 in /home/chauvo_a/PSU/minishell
**
** Made by Thibaut Chauvot
** Login   <chauvo_a@epitech.net>
**
** Started on  Wed Jan 20 16:56:56 2016 Thibaut Chauvot
** Last update Sun Feb 28 19:41:38 2016 Thibaut Chauvot
*/

#include <unistd.h>
#include <stdlib.h>
#include "include/sudoku.h"

char		*my_cat(char *str, char *buffer, int n)
{
  char		*tmp;

  if (n == 0)
    {
      if ((str = malloc(2)) == NULL)
	return (NULL);
      str[0] = buffer[0];
      str[1] = 0;
      return (str);
    }
  if (n > 0)
    {
      if ((tmp = malloc(n + 1)) == NULL)
	return (NULL);
      tmp = my_strcpy(tmp, str);
      tmp[n] = 0;
      if ((str = malloc(n + 2)) == NULL)
	return (NULL);
      my_strcpy(str, tmp);
      str[n] = buffer[0];
      str[n + 1] = 0;
    }
  free(tmp);
  return (str);
}

char		*get_line(const int fd)
{
  char		buffer[1] = "";
  char		*str;
  int		n;

  n = 0;
  while (buffer[0] != '\n')
    {
      if ((read(fd, buffer, 1)) <= 0)
	return (NULL);
      if ((str = my_cat(str, buffer, n)) == NULL)
	return (NULL);
      n++;
    }
  return (str);
}
