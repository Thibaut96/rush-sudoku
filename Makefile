NAME	=	sudoki-bi

CC	=	gcc

SRC	=	main.c \
		my_getnbr.c \
		sudok.c \
		check_tab.c \
		my_put_cpy.c \
		get_line.c

RM	=	rm -rf

OBJ	=	$(SRC:.c=.o)

all:		$(NAME)

$(NAME):		$(OBJ)
		$(CC) -o $(NAME) $(OBJ) -O0 -g -g3

clean:
		$(RM) $(OBJ)

fclean:		clean
		$(RM) $(NAME)

re:		fclean all

.PHONY:		all clean fclean re
